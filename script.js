//Obs: eu coloquei IDs nos botoes para poder pegar a posiçao correta que o usuario clica;

var tabela = new Array(0,0,0,0,0,0,0,0,0); // x = 1  o = 2;
var todos = new Array(0,0,0,0,0,0,0,0,0); // Array com os Ids dos botoes;
var resultado = new Array([0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]); // Array com todas as possibilidades possives;
var vez = 0;            //vez de quam vai jogar;
var contador = 0;       // Esse contador e pra checar a hora de chamar a funçao ganhou, pois um jogador so tem a possibilidade de ganhar a partir da tereira jogada;
var gameover = false;     // Essa váriavel serve pra ver se o jogo acabou, so pra pessoa nao poder clicar mais quando alguem ganhar.


// Realiza uma jogada
function selecionar(botao) {
    
    if(!gameover){
        if(vez == 0){
            if(tabela[botao.id.substring(1)] == 0){                 //Checa se a posiçao já esta preenchida para o usuario nao mudar a opção na mesma div;
                mudavez();                                         //Muda a vez do jogador;
                botao.innerHTML = 'X';                             //Coloca o X na div;
                tabela[botao.id.substring(1)] = 1;                 //Essa tabela guarda as posiçoes que foram clicadas, ex: o usuario clicou na posiçao 5,entao na posiçao 5 da tabela eu marco a opçao X ou O,dependendo da vez;
                
                if(contador > 3){                                   //Esse contador checa a hora de começar a ver quem ganhou, como so e possivel ter um vencedor a partir da 3 jogar entao a partir disso ele começa;
                ganhou(vez);
                }
                contador++;
            }  
        }else{
        if(tabela[botao.id.substring(1)] == 0){                   //Mesma coisa do que foi escrito acima;
                mudavez();
                botao.innerHTML = 'O';
                tabela[botao.id.substring(1)] = 2;
            
                if(contador > 3){
                    ganhou(vez);
                }  
                contador++;   
            }
        }
    }
}


// Zera todos as posições e recomeça o jogo
function resetar(){                                 //Essa função vai zerar tudo basicamento, desde as divs com os botoes até as variaveis globais;
    contador = 0;
    vez = 0;
    for(var i=0;i<tabela.length;i++){
        tabela[i] = 0;
    }
    for(var j=0;j<todos.length;j++){
        document.getElementsByClassName('casa')[j].innerHTML="";
    }
    document.getElementById('indicadorDaVez').innerHTML="";
    document.getElementById('indicadorVencedor').innerHTML=" ";
    gameover = false;
    
}


// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou(vez){

    for(var i in resultado){                                                                 //Esse laço vai percorrer o meu array de resultados que contém todas as possiveis combinaçoes para se ganhar o jogo;
        if(posicao(resultado[i],vez) == 'X'){                                               //Aqui irei checar se o resultado da minha funçao posiçao e igual a X, se for ele vai escrever o vencedor e parar o laço;
            document.getElementById('indicadorVencedor').innerHTML = 'O vencedor e X!';
            gameover = true;
            break;
        }else if(posicao(resultado[i],vez) == 'O'){
            document.getElementById('indicadorVencedor').innerHTML = 'O vencedor e O!';
            gameover = true;
            break;
        }else{
            if(tabela.indexOf(0) == -1){                                                    //Aqui eu checo se ainda há zeros no Array tabela que contem as jogadas, caso haja zeros siginifca que ainda há posiçoes para se jogar, caso contrario significa que o jogo acabou;
                document.getElementById('indicadorVencedor').innerHTML = 'Deu velha!';
            }
        }
    }  

}

 /*O vetor resultados e um Array de vetores, cada posiçao dele e um array com as posiçoes que levam a vitoria,
 essa função recebe como parâmetro um subvetor do vetor de resultados, que no caso sao posiçoes, e ele checa 
 se essas posiçoes na tabela contém 1 para X ou 2 para O, dependendo da vez de quem jogar. Se todas essas 
 posiçoes estiverem com algum desses dois elementos ele retorna o vencendor que e printado na funçao ganho();   
 */
function posicao(elemento,Vez){
    if(Vez == 1){
        if(tabela[elemento[0]] == 1 && tabela[elemento[1]] == 1 && tabela[elemento[2]] == 1){
            return 'X'
        }
    }else{
        if(tabela[elemento[0]] == 2 && tabela[elemento[1]] == 2 && tabela[elemento[2]] == 2){
            return 'O'
        }
    }
}


//Essa função muda a vez do jogador;
function mudavez(){
    if(vez == 0){
        var vezO =  document.getElementById('indicadorDaVez').innerHTML = 'O';
        vez += 1;
    }
    else if(vez == 1){
        var vezX =  document.getElementById('indicadorDaVez').innerHTML = 'X';
        vez -=1;   
     }
        
}
